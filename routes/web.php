<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/dmp/js/partner/partner.js', function () use ($router) {

  header("Content-Type: application/javascript");
  // $data = file_get_contents(public_path().'/js/dmp/partner.js');

  $data = file_get_contents('/home/forge/scribouille.fr/public/js/dmp/partner.js');
  echo $data;
  exit;

});

// Creation du cookie de base
$router->get('rdrct/{ndd_id}/{editor_id}/{hash}','DmpController@redirectGenCookie');

//Recréer le cookie sur le domaine de l'annonceur ayant fait appel à la fonction
$router->get('ajcal_coo', function () use ($router) {
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/javascript");
    $cookie_value = '';
    $origin_url = '';
    $to_be_hashed = "DMP-SC2CAP-VISITOR";
    $cookie_name = md5($to_be_hashed);
    if(isset($_COOKIE[$cookie_name])){
        $cookie_value = $_COOKIE[$cookie_name];

        $url = parse_url($_SERVER['HTTP_REFERER'],  PHP_URL_HOST);
        $subdomainPos = strpos($url, '.')+1;
        $origin_url = substr($url, $subdomainPos);

        // \Log::info("Value cookie:".$cookie_value);
        $data = json_decode($cookie_value);
        $data->origin = $origin_url;
        $cookie_value = json_encode($data);
        // \Log::info($origin_url);
    }

    echo "function setCookieTimeExpiration(){
            var now = new Date();
            var time = now.getTime();
            var expireTime = time + 365 * 3600 * 24 * 1000;
            now.setTime(expireTime);
            return now.toGMTString();
        }";

    if(strlen($cookie_value) > 0){
        echo "var get_cookie = '$cookie_name=$cookie_value;expires='+setCookieTimeExpiration()+';path=/;domain=$origin_url';";
        echo "\n";
        echo "document.cookie = get_cookie;";
    }
    exit;

});

// pour récupérer un acheteur avec un cookie sur le site partenaire
$router->get('ajcal_vis','DmpController@getVisitorData');




























// la suite sert à rien

$router->get('fabien', function () use ($router) {

$a = \DB::table('users')->get();

var_dump($a);
});



$router->get('test/{a}/{b}', 'DmpController@MaFct');


$router->get('/dmp/dev/js/partner/partner.js', function () use ($router) {

  header("Content-Type: application/javascript");

  echo 'var n = "";

/* prody2 */
/* @listNdd */

var lnd = ["t.damayhey.fr","t.dedidom.fr","t.lune-ette.fr","t.aeux.fr","t.alassaut.fr","t.otoilettes.fr","t.ilnousaime.fr","t.lune-a-tique.fr","t.jus-jonc.fr","t.site-rond.fr","t.cou-garre.fr","t.balle-tazar.fr","t.gigotons.fr","t.nos-ta-sion.fr","t.amort.fr","t.avue.fr","t.a-ri-peau-terre.fr","t.o-metro.fr","t.o-fayo.fr","t.i-ta-lit.fr","t.enarriere.fr"];

function getCe(cnm) {
    var nm = cnm + "=";
    var dc = document.cookie;
    var ca = dc.split(";");
    var i = 0;
    for(i; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == " ") {
            c = c.substring(1);
        }
        if (c.indexOf(nm) == 0) {
            return c.substring(nm.length, c.length);
        }
    }
    return "";
}

function gec(){

    if(getCe("8a704337bac01056fdb5ffdef3d6c57e") != ""){
      hoNew("8a704337bac01056fdb5ffdef3d6c57e",0);
    } else {
    lnd.forEach(function(el){
        var s =document.createElement("script");
        var u = "https://"+el+"/"+"ajcal_coo";
        s.setAttribute("type","text/javascript");
        s.setAttribute("src", u);
        document.getElementsByTagName("head")[0].appendChild(s);
        if(getCe("8a704337bac01056fdb5ffdef3d6c57e") != ""){
        //    n = el;
        //    ho("8a704337bac01056fdb5ffdef3d6c57e", 0);
        }else{
            document.getElementsByTagName("head")[0].removeChild(s);
        }
    });
    }
}

function gCD(){
    document.addEventListener("DOMContentLoaded",gec,false);
    document.addEventListener("DOMContentLoaded",gel, false);
}

function getXMLHttpRequest() {
    var xhr = null;
    if (window.XMLHttpRequest || window.ActiveXObject) {
        if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch(e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        } else {
            xhr = new XMLHttpRequest();
        }
    } else {
        console.log("Votre navigateur ne supporte pas objet XMLHTTPRequest...");
        return null;
    }
    return xhr;
}


function ho(t,ho){
    var cv = getCe(t);
    var h = window.location.href;
    if(cv != ""){
        var xhr = getXMLHttpRequest();
        xhr.onreadystatechange = function(){
            var response = xhr.responseText;
        }
        xhr.open("GET","https://"+n+"/ajcal_vis?token="+cv+"&has_ordered="+ho+"&by_link="+h,true);
        xhr.send();
    }
}

function gel(){
    var el = document.querySelectorAll("a[href*="https://"]");
    for(var i = 0;i< el.length;i++){
        el[i].addEventListener("click",function(){
            hoNew("8a704337bac01056fdb5ffdef3d6c57e",0);
        });
    }
}



function hoNew(t,ho){
    var cv = getCe(t);
    var h = window.location.href;
    if(cv != ""){
        var xhr = getXMLHttpRequest();
        xhr.onreadystatechange = function(){
            var response = xhr.responseText;
        }
        xhr.open("GET","https://scribouille.fr/ajcal_vis?token="+cv+"&has_ordered="+ho+"&by_link="+h,true);
        xhr.send();
    }
}


function gcC(){

        var s =document.createElement("script");
        var u = "https://scribouille.fr/"+"ajcal_coo";
        s.setAttribute("type","text/javascript");
        s.setAttribute("src", u);
        document.getElementsByTagName("head")[0].appendChild(s);
        if(getCe("8a704337bac01056fdb5ffdef3d6c57e") != ""){
            // n = el;
            hoNew("8a704337bac01056fdb5ffdef3d6c57e", 1);
        }else{
            document.getElementsByTagName("head")[0].removeChild(s);
        }

}';

});
