<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;

class DmpController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    //On génére le cookie sur le domaine de la DMP à l'ouverture du mail
    public function redirectGenCookie($ndd_id,$editor_id,$hash)
    {
        // \Log::info('GET PIXEL BY '. $hash .' - MD5');
        $toBeHashed = "DMP-SC2CAP-VISITOR";
        $cookieName = md5($toBeHashed);
        $lifetime = 3600 * 24 * 365;
        $data = array("hsh"=>$hash,
            "edt"=>$editor_id);
        $token = json_encode($data);

        $ndd = \DB::table('dmp_ndd')->where('id',$ndd_id)->first();
        if(!empty($ndd))
        {
            $newhit = $ndd->hit + 1;
           \DB::table('dmp_ndd')->where('id',$ndd_id)->update(['hit'=>$newhit]);
        }

        if(isset($_COOKIE[$cookieName])){

            $emptyOrNot = \DB::table('dmp_cookies')->where('hash',$hash)->where('editor_id',$editor_id)->first();

            if(is_null($emptyOrNot)){
                \DB::table('dmp_cookies')->where('hash',$hash)->insert([
                    'hash'=>$hash,
                    'editor_id'=>$editor_id,
                    'token' => $token,
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ]);
            }

            \DB::table('dmp_cookies')->where('hash',$hash)->where('editor_id',$editor_id)->update([
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
        }
        if(!isset($_COOKIE[$cookieName])){
            \DB::table('dmp_cookies')->where('hash',$hash)->insert([
                'hash'=>$hash,
                'editor_id'=>$editor_id,
                'token' => $token,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
        }
        setcookie($cookieName,$token,time()+$lifetime,'/');
        exit;
    }


    public function getVisitorData(){

      header('Access-Control-Allow-Origin: *');
      $data = json_decode(\Input::get('token'));
      $ajaxCallFrom = $data->origin;
      $partnerSite = \DB::table('dmp_partnersites')->where('url','LIKE',"%$data->origin%")->first();
      $destinataireInfo = \DB::table('dmp_cookies')->where('hash',$data->hsh)->where('editor_id',$data->edt)->first();

      if(\Input::get('by_link') !== null){
          \DB::table('dmp_tracking')->insert([
              'hash'=> $data->hsh,
              'partnersite'=> $partnerSite->id,
              'editor_id' => $data->edt,
              'link'=> \Input::get('by_link'),
              'updated_at'=>date('Y-m-d H:i:s'),
              'created_at'=>date('Y-m-d H:i:s')
          ]);
        }


      if(empty($partnerSite) ||empty($destinataireInfo)){
          exit;
      }
      $theme = \DB::table('themes')->where('id', $partnerSite->theme_id)->first();
      $originUrl = substr($partnerSite->url,0,-1);
      $destinataireExistence = \DB::table('dmp_matched')->where('destinataire_hash',$data->hsh)->where('base_site_id', $partnerSite->id)->first();

      if(empty($destinataireExistence)){
            $newMatchedId = \DB::table('dmp_matched')->insertGetId([
                'destinataire_hash'=>$data->hsh,
                'editor_id'=>$data->edt,
                'theme_id'=>$theme->id,
                // 'theme_id'=>1,
                'base_site_id'=>$partnerSite->id,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ]);
            $newDestinataire = \DB::table('dmp_matched')->where('id',$newMatchedId)->first();
            if(\Input::get('has_ordered') == 1){
                $value = $newDestinataire->nb_has_ordered++;
                \DB::table('dmp_matched')->where('id',$newMatchedId)->update(['nb_has_ordered'=>$value]);
            }
            /*
            if(\Input::get('has_ordered') == 0){
                $value = $newDestinataire->nb_has_ordered++;
                \DB::table('dmp_matched')->where('id',$newMatchedId)->update(['nb_has_ordered'=>$value]);
            }
            */
        }

        /*
        $hasOrdered = false;
        if(\Input::get('has_ordered') == 1){
            $hasOrdered = true;
        }

        $count_records = \DB::table('dmp_visitors')
        ->where('destinataire_hash',$data->hsh)
        ->where('base_site_id',$partnerSite->id)
        ->where('theme_id', $theme->id)
        ->where('editor_id',$data->edt)
        ->where('visited_at','LIKE',date('Y-m-d').'%')
        ->count();

        \Log::info("counter -- " . $count_records);

        if($count_records === 0){

        \DB::table('dmp_visitors')->insert([
            'destinataire_hash'=>$data->hsh,
            'visited_at'=>date('Y-m-d H:i:s'),
            'base_site_id'=>$partnerSite->id,
            'theme_id'=>$theme->id,
            'editor_id'=>$data->edt,
            'has_ordered'=>$hasOrdered
        ]);
        }
        */

        //TODO:A améliorer
        if(!empty($destinataireExistence)){
            if(\Input::get('has_ordered') == 1){
                $destinataireExistence->nb_has_ordered++;
                \DB::table('dmp_matched')
                    ->where('destinataire_hash',$data->hsh)
                    ->where('editor_id',$data->edt)
                    ->where('base_site_id',$partnerSite->id)
                    ->update(['nb_has_ordered' => $destinataireExistence->nb_has_ordered++]);
                // $destinataireExistence->save();
            }
            if(\Input::get('has_ordered') == 0){
                $destinataireExistence->nb_has_not_ordered++;
                \DB::table('dmp_matched')
                    ->where('destinataire_hash',$data->hsh)
                    ->where('editor_id',$data->edt)
                    ->where('base_site_id',$partnerSite->id)
                    ->update(['nb_has_not_ordered' => $destinataireExistence->nb_has_not_ordered++]);
                // $destinataireExistence->save();
            }
        }
        echo json_encode(['status'=>'ok']);
        exit;

    }





    public function MaFct($a, $b){
      echo $a;
      echo $b;
    }

}
